O:Today we did not learn any new content, but instead introduced a parking lot project as an exercise for the content we learned this week. During the exercise, the teacher led us step by step from requirement analysis, writing test cases, and finally using the "TDD" method to achieve the requirements.

R:I feel confused.

I:When we use the "TDD" approach to implement requirements, I often encounter a situation where the code that implements new and more advanced requirements passes new tests, but often cannot pass some outdated tests. But to submit a commit, you need to pass all the tests. So I have to keep some old code to pass those old tests, but these old codes are not only redundant, some even affect the execution efficiency of the code. This is where I am confused.

D:Today's code exercise has given me a deeper understanding of OOP and TDD ideas, but it has also raised some new questions. I hope to solve these questions as soon as possible and engage in new learning.