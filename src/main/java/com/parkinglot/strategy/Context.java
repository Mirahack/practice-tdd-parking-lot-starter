package com.parkinglot.strategy;

import com.parkinglot.entity.Car;
import com.parkinglot.entity.Ticket;

public class Context {

    private IParkingBoyStrategy strategy;

    public Context(IParkingBoyStrategy strategy) {
        this.strategy = strategy;
    }

    public Ticket executePark(Car car) {
        return strategy.park(car);
    }

    public Car executeFetch(Ticket ticket) {
        return strategy.fetch(ticket);
    }
}
