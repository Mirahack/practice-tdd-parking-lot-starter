package com.parkinglot.strategy;

import com.parkinglot.entity.Car;
import com.parkinglot.entity.ParkingLot;
import com.parkinglot.entity.Ticket;
import com.parkinglot.exception.NoAvailiblePositionException;
import com.parkinglot.exception.UnrecognizedTicketException;

import java.util.List;

public class SuperSmartParkingBoyOperation implements IParkingBoyStrategy {

    private List<ParkingLot> parkingLots;

    public SuperSmartParkingBoyOperation(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
    }

    @Override
    public Ticket park(Car car) {
        ParkingLot parkingLot = parkingLots.stream()
                .reduce((maxParkingLot, currentParkingLot) -> maxParkingLot.getPositionRate() >= currentParkingLot.getPositionRate() ? maxParkingLot : currentParkingLot)
                .orElse(null);
        if (parkingLot != null && parkingLot.getRemainCapacity() != 0) {
            return parkingLot.park(car);
        } else {
            throw new NoAvailiblePositionException();
        }
    }

    @Override
    public Car fetch(Ticket ticket) {
        for (ParkingLot parkingLot : parkingLots) {
            if (parkingLot.containsCar(ticket)) {
                return parkingLot.fetch(ticket);
            }
        }
        throw new UnrecognizedTicketException();
    }
}
