package com.parkinglot.strategy;


import com.parkinglot.entity.Car;
import com.parkinglot.entity.Ticket;

public interface IParkingBoyStrategy {
    Ticket park(Car car);

    Car fetch(Ticket ticket);
}
