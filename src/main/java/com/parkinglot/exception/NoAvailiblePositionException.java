package com.parkinglot.exception;

public class NoAvailiblePositionException extends RuntimeException {
    public NoAvailiblePositionException() {
        super("No available position");
    }
}
