package com.parkinglot.exception;

public class SendToFetchFailedException extends RuntimeException {
    public SendToFetchFailedException(String message) {
        super("manager send parking boy to fetch failed, the reason is " + message);
    }
}
