package com.parkinglot.exception;

public class ParkingBoyNotManageTheParkingLotExcepiton extends RuntimeException {
    public ParkingBoyNotManageTheParkingLotExcepiton() {
        super("parking boy doesn't manage the parking lot");
    }
}
