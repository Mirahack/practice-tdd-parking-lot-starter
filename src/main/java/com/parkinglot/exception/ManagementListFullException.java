package com.parkinglot.exception;

public class ManagementListFullException extends RuntimeException {
    public ManagementListFullException() {
        super("management list is full");
    }
}
