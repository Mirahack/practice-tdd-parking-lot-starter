package com.parkinglot.exception;

public class SendToParkFailedException extends RuntimeException {
    public SendToParkFailedException(String message) {
        super("manager send parking boy to park failed, the reason is " + message);
    }
}
