package com.parkinglot.entity;

import com.parkinglot.exception.NoAvailiblePositionException;

import java.util.List;

public class SuperSmartParkingBoy extends ParkingBoy {
    public SuperSmartParkingBoy(List<ParkingLot> parkingLot) {
        super(parkingLot);
    }

    @Override
    public Ticket park(Car car) {
        ParkingLot parkingLot = parkingLots.stream()
                .reduce((maxParkingLot, currentParkingLot) -> maxParkingLot.getPositionRate() >= currentParkingLot.getPositionRate() ? maxParkingLot : currentParkingLot)
                .orElse(null);
        if (parkingLot != null && parkingLot.getRemainCapacity() != 0) {
            return parkingLot.park(car);
        } else {
            throw new NoAvailiblePositionException();
        }
    }
}
