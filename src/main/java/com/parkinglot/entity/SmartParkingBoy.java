package com.parkinglot.entity;

import com.parkinglot.exception.NoAvailiblePositionException;

import java.util.List;

public class SmartParkingBoy extends ParkingBoy {
    public SmartParkingBoy(List<ParkingLot> parkingLots) {
        super(parkingLots);
    }

    @Override
    public Ticket park(Car car) {
        ParkingLot parkingLot = parkingLots.stream()
                .reduce((maxParkingLot, currentParkingLot) -> maxParkingLot.getRemainCapacity() >= currentParkingLot.getRemainCapacity() ? maxParkingLot : currentParkingLot)
                .orElse(null);
        if (parkingLot != null && parkingLot.getRemainCapacity() != 0) {
            return parkingLot.park(car);
        } else {
            throw new NoAvailiblePositionException();
        }
    }
}
