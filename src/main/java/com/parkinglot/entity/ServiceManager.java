package com.parkinglot.entity;

import com.parkinglot.exception.*;

import java.util.ArrayList;
import java.util.List;

public class ServiceManager extends ParkingBoy {

    private List<ParkingBoy> parkingBoys = new ArrayList<>(3);

    public ServiceManager(List<ParkingLot> parkingLots) {
        super(parkingLots);
    }

    public void addParkingBoys(ParkingBoy parkingBoy) {
        if (parkingBoys.size() == 3) {
            throw new ManagementListFullException();
        }
        parkingBoys.add(parkingBoy);
    }

    public Ticket sendToPark(ParkingBoy parkingBoy, Car car, ParkingLot parkingLot) {
        if (!parkingBoy.isManage(parkingLot)) {
            throw new ParkingBoyNotManageTheParkingLotExcepiton();
        }
        try {
            return parkingBoy.park(car);
        } catch (NoAvailiblePositionException e) {
            throw new SendToParkFailedException(e.getMessage());
        }
    }

    public Car sendToFetch(ParkingBoy parkingBoy, Ticket ticket, ParkingLot parkingLot) {
        if (!parkingBoy.isManage(parkingLot)) {
            throw new ParkingBoyNotManageTheParkingLotExcepiton();
        }
        try {
            return parkingBoy.fetch(ticket);
        } catch (UnrecognizedTicketException e) {
            throw new SendToFetchFailedException(e.getMessage());
        }
    }
}
