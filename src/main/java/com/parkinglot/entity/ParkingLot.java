package com.parkinglot.entity;

import com.parkinglot.exception.NoAvailiblePositionException;
import com.parkinglot.exception.UnrecognizedTicketException;

import java.util.HashMap;
import java.util.Map;

public class ParkingLot {
    private Map<Ticket, Car> relation;

    private int capacity;

    public ParkingLot(int capacity) {
        this.relation = new HashMap<>(capacity);
        this.capacity = capacity;
    }

    public Ticket park(Car car) {
        if (isNotFull()) {
            Ticket ticket = new Ticket();
            relation.put(ticket, car);
            return ticket;
        } else {
            throw new NoAvailiblePositionException();
        }
    }

    public Car fetch(Ticket ticket) {
        if (relation.containsKey(ticket)) {
            Car car = relation.get(ticket);
            relation.remove(ticket);
            return car;
        } else {
            throw new UnrecognizedTicketException();
        }
    }

    public Boolean containsCar(Ticket ticket) {
        return relation.containsKey(ticket);
    }

    public boolean isNotFull() {
        return capacity != relation.size();
    }

    public int getRemainCapacity() {
        return capacity - this.relation.size();
    }

    public double getPositionRate() {
        return (double) getRemainCapacity() / this.capacity;
    }
}
