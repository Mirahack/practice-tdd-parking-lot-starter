package com.parkinglot.entity;

import com.parkinglot.exception.NoAvailiblePositionException;
import com.parkinglot.exception.UnrecognizedTicketException;

import java.util.ArrayList;
import java.util.List;

public class ParkingBoy {
    protected List<ParkingLot> parkingLots;

    public ParkingBoy(ParkingLot parkingLot) {
        this.parkingLots = new ArrayList<>();
        this.parkingLots.add(parkingLot);
    }

    public ParkingBoy(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
    }

    public Ticket park(Car car) {
        for (ParkingLot parkingLot : parkingLots) {
            if (parkingLot.isNotFull()) {
                return parkingLot.park(car);
            }
        }
        throw new NoAvailiblePositionException();
    }

    public Car fetch(Ticket ticket) {
        for (ParkingLot parkingLot : parkingLots) {
            if (parkingLot.containsCar(ticket)) {
                return parkingLot.fetch(ticket);
            }
        }
        throw new UnrecognizedTicketException();
    }

    public Boolean isManage(ParkingLot parkingLot) {
        return this.parkingLots.contains(parkingLot);
    }
}
