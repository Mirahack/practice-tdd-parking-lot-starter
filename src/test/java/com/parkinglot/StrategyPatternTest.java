package com.parkinglot;

import com.parkinglot.entity.*;
import com.parkinglot.exception.NoAvailiblePositionException;
import com.parkinglot.strategy.Context;
import com.parkinglot.strategy.ParkingBoyOperation;
import com.parkinglot.strategy.SmartParkingBoyOperation;
import com.parkinglot.strategy.SuperSmartParkingBoyOperation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class StrategyPatternTest {
    @Test
    void should_return_properly_when_park_and_fetch_given_parking_boy_strategy_and_parking_lot_and_car() {
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(1);
        Context context = new Context(new ParkingBoyOperation(List.of(parkingLot)));
        Ticket ticket = context.executePark(car);

        Assertions.assertTrue(parkingLot.containsCar(ticket));

        Car fetchCar = context.executeFetch(ticket);

        Assertions.assertEquals(fetchCar, car);
    }

    @Test
    void should_return_specific_error_message_when_fetch_given_two_parking_lots_without_position_using_parking_boy_strategy() {
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot(0);
        Context context = new Context(new ParkingBoyOperation(List.of(parkingLot1, parkingLot2)));
        Car car = new Car();

        NoAvailiblePositionException noAvailiblePositionException = Assertions.assertThrows(NoAvailiblePositionException.class, () -> context.executePark(car));
        Assertions.assertEquals("No available position", noAvailiblePositionException.getMessage());
    }

    @Test
    void should_return_properly_when_park_and_fetch_given_smart_parking_boy_strategy_and_parking_lot_and_car() {
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(2);
        Context context = new Context(new SmartParkingBoyOperation(List.of(parkingLot1, parkingLot2)));
        Car car = new Car();

        Ticket ticket = context.executePark(car);

        Assertions.assertSame(true, parkingLot2.containsCar(ticket));

        Car fetchCar = context.executeFetch(ticket);

        Assertions.assertEquals(fetchCar, car);
    }

    @Test
    void should_return_specific_error_message_when_fetch_given_two_parking_lots_without_position_using_smart_parking_boy_strategy() {
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot(0);
        Context context = new Context(new SmartParkingBoyOperation(List.of(parkingLot1, parkingLot2)));
        Car car = new Car();

        NoAvailiblePositionException noAvailiblePositionException = Assertions.assertThrows(NoAvailiblePositionException.class, () -> context.executePark(car));
        Assertions.assertEquals("No available position", noAvailiblePositionException.getMessage());
    }

    @Test
    void should_return_properly_when_park_and_fetch_given_super_smart_parking_boy_strategy_and_parking_lot_and_car() {
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(5);
        Context context = new Context(new SuperSmartParkingBoyOperation(List.of(parkingLot1, parkingLot2)));
        Car car1 = new Car();
        context.executePark(car1);
        Car car2 = new Car();

        Ticket ticket = context.executePark(car2);

        Assertions.assertSame(true, parkingLot2.containsCar(ticket));

        Car fetchCar = context.executeFetch(ticket);

        Assertions.assertEquals(fetchCar, car2);
    }

    @Test
    void should_return_specific_error_message_when_fetch_given_two_parking_lots_without_position_using_super_smart_parking_boy_strategy() {
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot(0);
        Context context = new Context(new SuperSmartParkingBoyOperation(List.of(parkingLot1, parkingLot2)));
        Car car = new Car();

        NoAvailiblePositionException noAvailiblePositionException = Assertions.assertThrows(NoAvailiblePositionException.class, () -> context.executePark(car));
        Assertions.assertEquals("No available position", noAvailiblePositionException.getMessage());
    }

}
