package com.parkinglot;

import com.parkinglot.entity.*;
import com.parkinglot.exception.ManagementListFullException;
import com.parkinglot.exception.ParkingBoyNotManageTheParkingLotExcepiton;
import com.parkinglot.exception.SendToFetchFailedException;
import com.parkinglot.exception.SendToParkFailedException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class ServiceManagerTest {
    @Test
    void should_park_and_fetch_the_car_properly_when_manager_send_a_parking_boy_to_park_and_fetch_given_car_and_parking_lot_and_manager() {
        ServiceManager serviceManager = new ServiceManager(null);
        ParkingLot parkingLot = new ParkingLot(1);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        serviceManager.addParkingBoys(parkingBoy);
        Car car = new Car();

        Ticket ticket = serviceManager.sendToPark(parkingBoy, car, parkingLot);

        Assertions.assertTrue(parkingLot.containsCar(ticket));

        Car fetchCar = serviceManager.sendToFetch(parkingBoy, ticket, parkingLot);

        Assertions.assertEquals(fetchCar, car);
    }

    @Test
    void should_return_specific_error_message_when_manager_send_a_parking_boy_to_park_given_car_and_parking_lot_and_manager_and_parking_boy_which_do_not_manage_the_parking_lot() {
        ServiceManager serviceManager = new ServiceManager(null);
        ParkingLot parkingLot = new ParkingLot(1);
        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot(1));
        serviceManager.addParkingBoys(parkingBoy);
        Car car = new Car();

        ParkingBoyNotManageTheParkingLotExcepiton parkingBoyNotManageTheParkingLotExcepiton = Assertions.assertThrows(ParkingBoyNotManageTheParkingLotExcepiton.class, () -> serviceManager.sendToPark(parkingBoy, car, parkingLot));
        Assertions.assertEquals("parking boy doesn't manage the parking lot", parkingBoyNotManageTheParkingLotExcepiton.getMessage());
    }

    @Test
    void should_return_specific_error_message_when_manager_send_a_parking_boy_to_fetch_given_car_and_parking_lot_and_manager_and_parking_boy_which_do_not_manage_the_parking_lot() {
        ServiceManager serviceManager = new ServiceManager(null);
        ParkingLot parkingLot = new ParkingLot(1);
        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot(1));
        serviceManager.addParkingBoys(parkingBoy);
        Car car = new Car();
        Ticket ticket = parkingLot.park(car);

        ParkingBoyNotManageTheParkingLotExcepiton parkingBoyNotManageTheParkingLotExcepiton = Assertions.assertThrows(ParkingBoyNotManageTheParkingLotExcepiton.class, () -> serviceManager.sendToFetch(parkingBoy, ticket, parkingLot));
        Assertions.assertEquals("parking boy doesn't manage the parking lot", parkingBoyNotManageTheParkingLotExcepiton.getMessage());
    }

    @Test
    void should_park_and_fetch_the_car_properly_when_park_and_fetch_given_car_and_parking_lot_and_manager() {
        ParkingLot parkingLot = new ParkingLot(1);
        ServiceManager serviceManager = new ServiceManager(List.of(parkingLot));
        Car car = new Car();

        Ticket ticket = serviceManager.park(car);

        Assertions.assertTrue(parkingLot.containsCar(ticket));

        Car fetchCar = serviceManager.fetch(ticket);

        Assertions.assertEquals(car, fetchCar);
    }

    @Test
    void should_return_specific_error_message_when_manager_send_another_manager_to_park_given_car_and_parking_lot_and_manager_who_does_not_manage_it() {
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ServiceManager serviceManager1 = new ServiceManager(null);
        ServiceManager serviceManager2 = new ServiceManager(List.of(parkingLot2));
        serviceManager1.addParkingBoys(serviceManager2);
        Car car = new Car();

        ParkingBoyNotManageTheParkingLotExcepiton parkingBoyNotManageTheParkingLotExcepiton = Assertions.assertThrows(ParkingBoyNotManageTheParkingLotExcepiton.class, () -> serviceManager1.sendToPark(serviceManager2, car, parkingLot1));
        Assertions.assertEquals("parking boy doesn't manage the parking lot", parkingBoyNotManageTheParkingLotExcepiton.getMessage());
    }

    @Test
    void should_return_specific_error_message_when_manager_send_another_manager_to_fetch_given_car_and_parking_lot_and_manager_who_does_not_manage_it() {
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ServiceManager serviceManager1 = new ServiceManager(null);
        ServiceManager serviceManager2 = new ServiceManager(List.of(parkingLot2));
        serviceManager1.addParkingBoys(serviceManager2);
        Car car = new Car();
        Ticket ticket = parkingLot1.park(car);

        ParkingBoyNotManageTheParkingLotExcepiton parkingBoyNotManageTheParkingLotExcepiton = Assertions.assertThrows(ParkingBoyNotManageTheParkingLotExcepiton.class, () -> serviceManager1.sendToFetch(serviceManager2, ticket, parkingLot1));
        Assertions.assertEquals("parking boy doesn't manage the parking lot", parkingBoyNotManageTheParkingLotExcepiton.getMessage());
    }

    @Test
    void should_return_specific_error_message_when_manager_send_parking_boy_to_park_but_failed_given_manager_and_parking_boy_and_car_and_full_parking_lot() {
        ServiceManager serviceManager = new ServiceManager(null);
        ParkingLot parkingLot = new ParkingLot(0);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        serviceManager.addParkingBoys(parkingBoy);
        Car car = new Car();

        SendToParkFailedException sendToParkFailedException = Assertions.assertThrows(SendToParkFailedException.class, () -> serviceManager.sendToPark(parkingBoy, car, parkingLot));
        Assertions.assertEquals("manager send parking boy to park failed, the reason is No available position", sendToParkFailedException.getMessage());
    }

    @Test
    void should_return_specific_error_message_when_manager_send_parking_boy_to_fetch_but_failed_given_manager_and_parking_boy_and_parking_lot_which_has_car_and_invalid_or_used_ticket() {
        ServiceManager serviceManager = new ServiceManager(null);
        ParkingLot parkingLot = new ParkingLot(1);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        serviceManager.addParkingBoys(parkingBoy);
        Car car = new Car();
        Ticket ticket = serviceManager.sendToPark(parkingBoy, car, parkingLot);
        serviceManager.sendToFetch(parkingBoy, ticket, parkingLot);

        SendToFetchFailedException sendToFetchFailedException1 = Assertions.assertThrows(SendToFetchFailedException.class, () -> serviceManager.sendToFetch(parkingBoy, ticket, parkingLot));
        Assertions.assertEquals("manager send parking boy to fetch failed, the reason is unrecognized ticket", sendToFetchFailedException1.getMessage());

        SendToFetchFailedException sendToFetchFailedException2 = Assertions.assertThrows(SendToFetchFailedException.class, () -> serviceManager.sendToFetch(parkingBoy, new Ticket(), parkingLot));
        Assertions.assertEquals("manager send parking boy to fetch failed, the reason is unrecognized ticket", sendToFetchFailedException2.getMessage());
    }

    @Test
    void should_return_specific_error_message_when_manager_add_parking_boy_given_parking_boy_and_manager_which_management_list_is_full() {
        ServiceManager serviceManager = new ServiceManager(null);
        ParkingLot parkingLot = new ParkingLot(1);
        for (int i = 0; i < 3; i++) {
            ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
            serviceManager.addParkingBoys(parkingBoy);
        }
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);

        ManagementListFullException managementListFullException = Assertions.assertThrows(ManagementListFullException.class, () -> serviceManager.addParkingBoys(parkingBoy));
        Assertions.assertEquals("management list is full", managementListFullException.getMessage());
    }

}
