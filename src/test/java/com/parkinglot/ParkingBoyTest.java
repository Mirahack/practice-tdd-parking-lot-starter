package com.parkinglot;

import com.parkinglot.entity.Car;
import com.parkinglot.entity.ParkingBoy;
import com.parkinglot.entity.ParkingLot;
import com.parkinglot.entity.Ticket;
import com.parkinglot.exception.NoAvailiblePositionException;
import com.parkinglot.exception.UnrecognizedTicketException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class ParkingBoyTest {
    @Test
    void should_return_ticket_when_park_given_parking_lot_and_parking_boy_and_car() {
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        Car car = new Car();

        Ticket ticket = parkingBoy.park(car);

        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_car_when_fetch_given_parking_lot_with_a_parked_car_and_parking_boy() {
        ParkingLot parkingLot = new ParkingLot(1);
        Car car = new Car();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        Ticket ticket = parkingBoy.park(car);

        Car fetchCar = parkingBoy.fetch(ticket);

        Assertions.assertEquals(fetchCar, car);
    }

    @Test
    void should_return_each_car_right_when_fetch_twice_given_parking_lot_with_two_parked_car_and_parking_boy() {
        ParkingLot parkingLot = new ParkingLot(2);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        Car car1 = new Car();
        Ticket ticket1 = parkingBoy.park(car1);
        Car car2 = new Car();
        Ticket ticket2 = parkingBoy.park(car2);

        Car fetchCar1 = parkingBoy.fetch(ticket1);
        Car fetchCar2 = parkingBoy.fetch(ticket2);

        Assertions.assertEquals(fetchCar1, car1);
        Assertions.assertEquals(fetchCar2, car2);
    }

    @Test
    void should_throw_exception_with_specific_message_when_fetch_given_parking_lot_and_wrong_ticket_and_parking_boy() {
        ParkingLot parkingLot = new ParkingLot(1);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        Ticket ticket = new Ticket();

        UnrecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnrecognizedTicketException.class, () -> parkingBoy.fetch(ticket));
        Assertions.assertEquals("unrecognized ticket", unrecognizedTicketException.getMessage());
    }

    @Test
    void should_throw_exception_with_specific_message_when_fetch_given_parking_lot_and_used_ticket_and_parking_boy() {
        ParkingLot parkingLot = new ParkingLot(1);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        Car car = new Car();
        Ticket ticket = parkingBoy.park(car);
        parkingBoy.fetch(ticket);

        UnrecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnrecognizedTicketException.class, () -> parkingBoy.fetch(ticket));
        Assertions.assertEquals("unrecognized ticket", unrecognizedTicketException.getMessage());
    }

    @Test
    void should_throw_exception_with_specific_message_when_fetch_given_parking_lot_without_position_and_parking_boy() {
        ParkingLot parkingLot = new ParkingLot(0);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        Car car = new Car();

        NoAvailiblePositionException noAvailiblePositionException = Assertions.assertThrows(NoAvailiblePositionException.class, () -> parkingBoy.park(car));
        Assertions.assertEquals("No available position", noAvailiblePositionException.getMessage());
    }

    @Test
    void should_the_car_park_the_first_parking_lot_when_park_given_two_parking_lot_and_parking_boy_and_car() {
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot1, parkingLot2));
        Car car = new Car();

        Ticket ticket = parkingBoy.park(car);

        Assertions.assertSame(true, parkingLot1.containsCar(ticket));
    }

    @Test
    void should_the_car_park_the_second_parking_lot_when_given_two_parking_lot_with_the_first_full_and_parking_boy_and_car() {
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot1, parkingLot2));
        Car car = new Car();

        Ticket ticket = parkingBoy.park(car);

        Assertions.assertSame(true, parkingLot2.containsCar(ticket));
    }

    @Test
    void should_return_the_right_car_with_each_ticket_when_fetch_twice_given_two_parking_lot_both_with_parked_car_and_two_tickets() {
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot1, parkingLot2));
        Car car1 = new Car();
        Ticket ticket1 = parkingBoy.park(car1);
        Car car2 = new Car();
        Ticket ticket2 = parkingBoy.park(car2);

        Car fetchCar1 = parkingBoy.fetch(ticket1);
        Car fetchCar2 = parkingBoy.fetch(ticket2);

        Assertions.assertSame(car1, fetchCar1);
        Assertions.assertSame(car2, fetchCar2);
    }

    @Test
    void should_return_specific_error_message_when_fetch_given_two_parking_lots_and_parking_boy_and_unrecognized_ticket() {
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot1, parkingLot2));
        Ticket ticket = new Ticket();

        UnrecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnrecognizedTicketException.class, () -> parkingBoy.fetch(ticket));
        Assertions.assertEquals("unrecognized ticket", unrecognizedTicketException.getMessage());
    }

    @Test
    void should_return_specific_error_message_when_fetch_given_two_parking_lots_and_parking_boy_and_used_ticket() {
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot1, parkingLot2));
        Car car = new Car();
        Ticket ticket = parkingBoy.park(car);

        parkingBoy.fetch(ticket);

        UnrecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnrecognizedTicketException.class, () -> parkingBoy.fetch(ticket));
        Assertions.assertEquals("unrecognized ticket", unrecognizedTicketException.getMessage());
    }

    @Test
    void should_return_specific_error_message_when_fetch_given_two_parking_lots_without_position() {
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot(0);
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot1, parkingLot2));
        Car car = new Car();

        NoAvailiblePositionException noAvailiblePositionException = Assertions.assertThrows(NoAvailiblePositionException.class, () -> parkingBoy.park(car));
        Assertions.assertEquals("No available position", noAvailiblePositionException.getMessage());
    }
}
