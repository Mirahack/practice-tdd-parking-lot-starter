package com.parkinglot;

import com.parkinglot.entity.Car;
import com.parkinglot.entity.ParkingLot;
import com.parkinglot.entity.Ticket;
import com.parkinglot.exception.NoAvailiblePositionException;
import com.parkinglot.exception.UnrecognizedTicketException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParkingLotTest {
    @Test
    void should_return_ticket_when_park_given_parking_lot() {
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car();

        Ticket ticket = parkingLot.park(car);

        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_car_when_fetch_given_parking_lot_with_a_parked_car() {
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car();
        Ticket ticket = parkingLot.park(car);

        Car fetchCar = parkingLot.fetch(ticket);

        Assertions.assertEquals(fetchCar, car);
    }

    @Test
    void should_return_each_car_right_when_fetch_twice_given_parking_lot_with_two_parked_car() {
        ParkingLot parkingLot = new ParkingLot(10);
        Car car1 = new Car();
        Ticket ticket1 = parkingLot.park(car1);
        Car car2 = new Car();
        Ticket ticket2 = parkingLot.park(car2);

        Car fetchCar1 = parkingLot.fetch(ticket1);
        Car fetchCar2 = parkingLot.fetch(ticket2);

        Assertions.assertEquals(fetchCar1, car1);
        Assertions.assertEquals(fetchCar2, car2);
    }

    @Test
    void should_throw_exception_with_specific_message_when_fetch_given_parking_lot_and_wrong_ticket() {
        ParkingLot parkingLot = new ParkingLot(1);
        Ticket ticket = new Ticket();

        UnrecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnrecognizedTicketException.class, () -> parkingLot.fetch(ticket));
        Assertions.assertEquals("unrecognized ticket", unrecognizedTicketException.getMessage());
    }

    @Test
    void should_throw_exception_with_specific_message_when_fetch_given_parking_lot_and_used_ticket() {
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car();
        Ticket ticket = parkingLot.park(car);
        Car fetchCar = parkingLot.fetch(ticket);

        UnrecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnrecognizedTicketException.class, () -> parkingLot.fetch(ticket));
        Assertions.assertEquals("unrecognized ticket", unrecognizedTicketException.getMessage());
    }

    @Test
    void should_throw_exception_with_specific_message_when_fetch_given_parking_lot_without_position() {
        ParkingLot parkingLot = new ParkingLot(0);
        Car car = new Car();

        NoAvailiblePositionException noAvailiblePositionException = Assertions.assertThrows(NoAvailiblePositionException.class, () -> parkingLot.park(car));
        Assertions.assertEquals("No available position", noAvailiblePositionException.getMessage());
    }

}
