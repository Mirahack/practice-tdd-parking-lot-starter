package com.parkinglot;

import com.parkinglot.entity.Car;
import com.parkinglot.entity.ParkingLot;
import com.parkinglot.entity.SmartParkingBoy;
import com.parkinglot.entity.Ticket;
import com.parkinglot.exception.NoAvailiblePositionException;
import com.parkinglot.exception.UnrecognizedTicketException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class SmartParkingBoyTest {

    @Test
    void should_the_car_park_the_second_parking_lot_when_park_given_two_parking_lot_with_the_second_has_more_position_and_parking_boy_and_car() {
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(2);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(List.of(parkingLot1, parkingLot2));
        Car car = new Car();

        Ticket ticket = smartParkingBoy.park(car);

        Assertions.assertSame(true, parkingLot2.containsCar(ticket));
    }

    @Test
    void should_the_car_park_the_second_parking_lot_when_given_two_parking_lot_with_the_first_full_and_smart_parking_boy_and_car() {
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot(1);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(List.of(parkingLot1, parkingLot2));
        Car car = new Car();

        Ticket ticket = smartParkingBoy.park(car);

        Assertions.assertSame(true, parkingLot2.containsCar(ticket));
    }

    @Test
    void should_return_the_right_car_with_each_ticket_when_fetch_twice_given_two_parking_lot_both_with_parked_car_and_two_tickets_and_smart_parking_boy() {
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(List.of(parkingLot1, parkingLot2));
        Car car1 = new Car();
        Ticket ticket1 = smartParkingBoy.park(car1);
        Car car2 = new Car();
        Ticket ticket2 = smartParkingBoy.park(car2);

        Car fetchCar1 = smartParkingBoy.fetch(ticket1);
        Car fetchCar2 = smartParkingBoy.fetch(ticket2);

        Assertions.assertSame(car1, fetchCar1);
        Assertions.assertSame(car2, fetchCar2);
    }

    @Test
    void should_return_specific_error_message_when_fetch_given_two_parking_lots_and_smart_parking_boy_and_unrecognized_ticket() {
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(List.of(parkingLot1, parkingLot2));
        Ticket ticket = new Ticket();

        UnrecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnrecognizedTicketException.class, () -> smartParkingBoy.fetch(ticket));
        Assertions.assertEquals("unrecognized ticket", unrecognizedTicketException.getMessage());
    }

    @Test
    void should_return_specific_error_message_when_fetch_given_two_parking_lots_and_smart_parking_boy_and_used_ticket() {
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(List.of(parkingLot1, parkingLot2));
        Car car = new Car();
        Ticket ticket = smartParkingBoy.park(car);

        smartParkingBoy.fetch(ticket);

        UnrecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnrecognizedTicketException.class, () -> smartParkingBoy.fetch(ticket));
        Assertions.assertEquals("unrecognized ticket", unrecognizedTicketException.getMessage());
    }

    @Test
    void should_return_specific_error_message_when_fetch_given_two_parking_lots_without_position() {
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot(0);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(List.of(parkingLot1, parkingLot2));
        Car car = new Car();

        NoAvailiblePositionException noAvailiblePositionException = Assertions.assertThrows(NoAvailiblePositionException.class, () -> smartParkingBoy.park(car));
        Assertions.assertEquals("No available position", noAvailiblePositionException.getMessage());
    }
}
