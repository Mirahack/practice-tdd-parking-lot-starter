package com.parkinglot;

import com.parkinglot.entity.Car;
import com.parkinglot.entity.ParkingLot;
import com.parkinglot.entity.SuperSmartParkingBoy;
import com.parkinglot.entity.Ticket;
import com.parkinglot.exception.NoAvailiblePositionException;
import com.parkinglot.exception.UnrecognizedTicketException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class SuperSmartParkingBoyTest {
    @Test
    void should_the_car_park_the_second_parking_lot_when_park_given_two_parking_lot_with_the_second_has_higher_position_rate_and_parking_boy_and_car() {
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(5);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(List.of(parkingLot1, parkingLot2));
        Car car1 = new Car();
        superSmartParkingBoy.park(car1);
        Car car2 = new Car();

        Ticket ticket = superSmartParkingBoy.park(car2);

        Assertions.assertSame(true, parkingLot2.containsCar(ticket));
    }

    @Test
    void should_the_car_park_the_second_parking_lot_when_given_two_parking_lot_with_the_first_full_and_parking_boy_and_car() {
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot(1);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(List.of(parkingLot1, parkingLot2));
        Car car = new Car();

        Ticket ticket = superSmartParkingBoy.park(car);

        Assertions.assertSame(true, parkingLot2.containsCar(ticket));
    }

    @Test
    void should_return_the_right_car_with_each_ticket_when_fetch_twice_given_two_parking_lot_both_with_parked_car_and_two_tickets_and_super_smart_parking_boy() {
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(List.of(parkingLot1, parkingLot2));
        Car car1 = new Car();
        Ticket ticket1 = superSmartParkingBoy.park(car1);
        Car car2 = new Car();
        Ticket ticket2 = superSmartParkingBoy.park(car2);

        Car fetchCar1 = superSmartParkingBoy.fetch(ticket1);
        Car fetchCar2 = superSmartParkingBoy.fetch(ticket2);

        Assertions.assertSame(car1, fetchCar1);
        Assertions.assertSame(car2, fetchCar2);
    }

    @Test
    void should_return_specific_error_message_when_fetch_given_two_parking_lots_and_super_smart_parking_boy_and_unrecognized_ticket() {
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(List.of(parkingLot1, parkingLot2));
        Ticket ticket = new Ticket();

        UnrecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnrecognizedTicketException.class, () -> superSmartParkingBoy.fetch(ticket));
        Assertions.assertEquals("unrecognized ticket", unrecognizedTicketException.getMessage());
    }

    @Test
    void should_return_specific_error_message_when_fetch_given_two_parking_lots_and_super_smart_parking_boy_and_used_ticket() {
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(List.of(parkingLot1, parkingLot2));
        Car car = new Car();
        Ticket ticket = superSmartParkingBoy.park(car);

        superSmartParkingBoy.fetch(ticket);

        UnrecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnrecognizedTicketException.class, () -> superSmartParkingBoy.fetch(ticket));
        Assertions.assertEquals("unrecognized ticket", unrecognizedTicketException.getMessage());
    }

    @Test
    void should_return_specific_error_message_when_fetch_given_two_parking_lots_without_position_and_super_smart_parking_boy() {
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot(0);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(List.of(parkingLot1, parkingLot2));
        Car car = new Car();

        NoAvailiblePositionException noAvailiblePositionException = Assertions.assertThrows(NoAvailiblePositionException.class, () -> superSmartParkingBoy.park(car));
        Assertions.assertEquals("No available position", noAvailiblePositionException.getMessage());
    }
}
